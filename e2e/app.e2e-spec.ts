import { CiDemoPage } from './app.po';

describe('ci-demo App', () => {
  let page: CiDemoPage;

  beforeEach(() => {
    page = new CiDemoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
